<?php

//не смог понять как работать с номерами в таблице на страницах админа/учителей/студентов

$info = [
    [
        'name' => 'Admin10 Admin',
        'group' => 'administrator',
        'email' => 'Admin_Admin@gmail.com',
        'phone' => '1234567',
    ],
    [
        'name' => 'Teach1 AAAAA',
        'group' => 'teachers',
        'email' => 'Teach1_AAAAA@gmail.com',
        'phone' => '1111111',
    ],
    [
        'name' => 'Teach2 BBBBB',
        'group' => 'teachers',
        'email' => 'Teach2_BBBBB@gmail.com',
        'phone' => '2222222',
    ],
    [
        'name' => 'Teach3 CCCCC',
        'group' => 'teachers',
        'email' => 'Teach3_CCCCC@gmail.com',
        'phone' => '3333333',
    ],
    [
        'name' => 'Stud4 DDDDD',
        'group' => 'students',
        'email' => 'Stud4_DDDDD@gmail.com',
        'phone' => '4444444',
    ],
    [
        'name' => 'Stud5 EEEEE',
        'group' => 'students',
        'email' => 'Stud5_EEEEE@gmail.com',
        'phone' => '5555555',
    ],
    [
        'name' => 'Stud6 FFFFF',
        'group' => 'students',
        'email' => 'Stud6_FFFFF@gmail.com',
        'phone' => '6666666',
    ],
    [
        'name' => 'Stud7 GGGGG',
        'group' => 'students',
        'email' => 'Stud7_GGGGG@gmail.com',
        'phone' => '7777777',
    ],
    [
        'name' => 'Stud8 HHHHH',
        'group' => 'students',
        'email' => 'Stud8_HHHHH@gmail.com',
        'phone' => '8888888',
    ],
    [
        'name' => 'Stud9 IIIII',
        'group' => 'students',
        'email' => 'Stud9_IIIII@gmail.com',
        'phone' => '9999999',
    ], 
];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>
<body>
    <div class="links">
        <ul>
            <li>
                <a href="/administrator.php">administrator</a>
            </li>
            <li>
                <a href="/teachers.php">teachers</a>
            </li>
            <li>
                <a href="/students.php">students</a>
            </li>
        </ul>
    </div>
<div class="container">
        <div class="row col-12">
                <h2>Authorization:</h2>
                <form action="/" method="POST">
                    <div class="form-group">
                        <label>name:
                        <input type="text" name="name" class="form-control">
                        </label>
                    </div>
                    <div class="form-group">
                        <label>group:
                        <select multiple class="form-control" name="group">
                            <option>administrator</option>
                            <option>teachers</option>
                            <option>students</option>
                        </select>
                        </label>
                    </div>
                    <div class="form-group">
                        <label>email:
                        <input type="email" name="email" class="form-control">
                        </label>
                    </div>
                    <div class="form-group">
                        <label>phone:
                        <input type="text" name="phone" class="form-control">
                        </label>
                    </div>
                    <div class="form-group">
                        <label>age:
                        <input type="text" name="age" class="form-control">
                        </label>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success">push</button>
                    </div>
                </form>
            <div>
                <p>
                    <?php if( (!empty($_POST['name'])) && (!empty($_POST['group'])) && (!empty($_POST['email'])) && (!empty($_POST['phone'])) && (!empty($_POST['age'])) ):?>
                        <?php if (  ($_POST['age'] >= 18 ) ):?>
                            <?php $insertedInfo = $_POST;?>
                        <?php else:?>
                            Вы слишком молоды
                        <?php endif?>
                        <?php else:?>                      
                    Введите полные данные
                    <?php endif?>
                </p>
            </div>
            <div>
                <p>
                <?php if (  ($insertedInfo['group'] == 'administrator' ) ):?>
                    Поздравляем, вы успешно добавлены в группу администраторы
                <?php elseif (  ($insertedInfo['group'] == 'teachers' ) ):?>   
                    Поздравляем, вы успешно добавлены в группу учителя
                <?php elseif (  ($insertedInfo['group'] == 'students' ) ):?>   
                    Поздравляем, вы успешно добавлены в группу студенты
                <?php endif?>
                </p>
            </div>
        </div>
        <hr>
    <div class="container">
        <div class="row col-6">
                <table class="table table-striped">
                    <tr>
                        <th>#</th>
                        <th>name</th>
                        <th>group</th>
                        <th>email</th>
                        <th>phone</th>
                    </tr>
                    <?php foreach($info as $key => $inf):?>
                        <tr>
                            <td><?=++$key?></td>
                            <td><?=$inf['name']?></td>
                            <td><?=$inf['group']?></td>
                            <td><?=$inf['email']?></td>
                            <td><?=$inf['phone']?></td>
                        </tr>
                    <?php endforeach;?>
                        <tr>
                            <td><?=++$key?></td>
                            <td><?=$insertedInfo['name']?></td>
                            <td><?=$insertedInfo['group']?></td>
                            <td><?=$insertedInfo['email']?></td>
                            <td><?=$insertedInfo['phone']?></td>
                        </tr>
                </table>
        </div>
    </div>    
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>
</html>